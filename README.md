splunk-forwarder
=================

This role deploys a Splunk Universal Forwarder. 

Requirements
------------

Tested with Ansible 1.8 on Ubuntu 14.04 64-bit.

Role Variables
--------------

    splunk_download_url		# Location of Splunk install file
    inputs			# List of paths to monitor
    output			# Where to forward data
    			

Dependencies
------------


Example Playbook
----------------


```
#!yaml

- hosts: servers
  sudo: yes
  roles: 
    - role: splunk-forwarder
      inputs:
        - path: /var/log/nginx.log
          config:
            sourcetype: access_combined
        - path: /var/log/syslog
          config:
            sourcetype: syslog
      output:
        name: default_indexer
        config:
          server: indexer.mydomain.com:9997
          compressed: true
          sslCertPath: "$SPLUNK_HOME/etc/auth/server.pem"
          sslRootCAPath: "$SPLUNK_HOME/etc/auth/cacert.pem"
          sslVerifyServerCert: false
          sslPassword: password

```


License
-------

BSD

Author Information
------------------

sam.kitonyi@gmail.com